package com.project.challange.Model;

import javax.persistence.*;

@Entity
@Table(name ="user")
public class Register {

//    membuat column id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    membuat column username
    @Column (name = "username")
    private  String username;

//    membuat column email
    @Column (name = "email")
    private String email;

//    membuat column password
    @Column (name = "password")
    private  String password;

//    membuat construktor kosong
    public Register() {
    }

//    membuat contruktor tanpa id
    public Register(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }

//    membuat getter setter id
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

//    membuat getter setter username
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

//    membuat getter setter email
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

//    membuat getter setter password
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
