package com.project.challange.Controller;

import com.project.challange.Model.Register;
import com.project.challange.Service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "http://localhost:3001")
public class RegisterController {

    @Autowired
    RegisterService registerService;

    @GetMapping("/all") //untuk melihat semua data
    public List<Register> getAllUser() {
        return registerService.getAllUser();
    }

    @GetMapping("/{id}") //untuk melihat sesaui id
    public Register getUserById(@PathVariable("id")Long id) {
        return (registerService.getUserById(id)) ;
    }

    @PostMapping // untuk mengepost data / untuk register
    public Register addUser(@RequestBody Register register) {
        return registerService.addUser(register);
    }

    @PostMapping("/sign-in") // untuk mengepost data / untuk login
    public Map<String, Object> login(@RequestBody Register register) {
        return registerService.login(register);
    }
    @PutMapping("/{id}") // untuk mengedit data sesuai id
    public Register editUserById(@PathVariable("id") Long id, @RequestBody Register register) {
        return registerService.editUserById(id, register);
    }

    @DeleteMapping("/{id}") // untuk menghapus data sesuai id
    public void deleteUSerById(@PathVariable("id") Long id) {
        registerService.deleteUserById(id);}
}
