package com.project.challange.Service;


import com.project.challange.Model.Register;
import com.project.challange.Repository.RegisterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class RegisterServicelmpl implements RegisterService{

    @Autowired
    RegisterRepository registerRepository; // memanggil register repository


    @Override
    public Map<String, Object> login(Register register) {
        Register register1 = registerRepository.findByEmail(register.getEmail()).get();
        Map<String, Object> response = new HashMap<>();
        response.put("user", register1);
        return response;
    }

    @Override
    public Register addUser(Register register) { // membuat method add supaya bisa mengepost / menambahkan data
//       Register register1 = new Register(register.setPassword(register.getPassword()(register.getUsername())
        register.setEmail(register.getEmail());
        return registerRepository.save(register);
    }

    @Override
    public Register getUserById(Long id) { // membuat method get id supaya bisa melihat sesaui dgn id yg diinginkan
        return registerRepository.findById(id).orElseThrow();
    }

    @Override
    public List<Register> getAllUser() { // membuat get all supaya bisa melihat semua data
        return registerRepository.findAll();
    }

    @Override
    public Register editUserById(Long id,Register register) { // membuat method edit supaya bisa mengedit data sebelumnya
        Register update = registerRepository.findById(id).get();
        update.setUsername(register.getUsername());
        update.setEmail(register.getEmail());
        update.setPassword(register.getPassword());
        return registerRepository.save(update);
    }

    @Override
    public Map<String, Boolean> deleteUserById(Long id) { // membuat method delete supaya bisa menghapus data yg sudah tidak digunakan
            registerRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;

    }
}
