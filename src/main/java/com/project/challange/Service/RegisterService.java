package com.project.challange.Service;

import com.project.challange.Model.Register;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface RegisterService {
    Map<String, Object> login(Register register);  //untuk login
    Register addUser (Register register); //method add untuk mengepost
    Register getUserById (Long id); // method untuk melihat sesuaiid yg dicari
    List<Register> getAllUser(); // method untuk melihat semua data
    Register editUserById(Long id, Register register); // method untuk mengedit data sesuai id
    Map<String,Boolean> deleteUserById(Long id); // method untuk mendelete data sesaui data
}
