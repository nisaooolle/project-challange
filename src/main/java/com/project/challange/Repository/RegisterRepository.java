package com.project.challange.Repository;

import com.project.challange.Model.Register;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RegisterRepository extends JpaRepository<Register,Long> {
    Optional<Register> findByEmail(String email); // membuat validasi jika email sudah digunakan
}
